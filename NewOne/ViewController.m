//
//  ViewController.m
//  NewOne
//
//  Created by Rakesh Kumar on 14/06/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "ProgressHUD.h"

#define HideProgressHUD [ProgressHUD dismiss];
#define ShowProgressHUD [ProgressHUD show:@"Please wait..."];

#define typeTf 1
#define brandTf 2

#define bottlePriceTf 3
#define bottlePackTf 4

#define canPriceTf 5
#define canPackTf 6

#define adjcentTf 7
#define doorsTf 8

#define posTf 9
#define typeOfAccountTf 10

int i;

NSString *filePath;

NSMutableArray *InfoArray;

MFMailComposeViewController *comp;
NSMutableDictionary *dict;


@interface ViewController ()

@end

@implementation ViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    float sizeOfContent = 0;
    
    NSInteger wd = self.view.frame.size.height;
    NSInteger ht = _scrollView.frame.size.height;
    
    InfoArray  = [[NSMutableArray alloc]initWithCapacity:0];

    sizeOfContent = ht + 380 ;
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, sizeOfContent);
    
    dataArray = [NSMutableArray new];
    [PickerView setHidden:YES];
    
    [segmentControl addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
    segmentControl.selectedSegmentIndex = 1;

    [segmentControl1 addTarget:self action:@selector(segmentAction1:) forControlEvents: UIControlEventValueChanged];
    segmentControl1.selectedSegmentIndex = 1;
    onSale = [NSString stringWithFormat:@"No"];
    eyeLevel = [NSString stringWithFormat:@"No"];
    
}


- (void)segmentAction:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex)
    {
        case 0:
            onSale = [NSString stringWithFormat:@"Yes"];
            break;
        case 1:
            onSale = [NSString stringWithFormat:@"No"];
            break;
        default:
            break;
    }
}


- (void)segmentAction1:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex)
    {
        case 0:
             eyeLevel = [NSString stringWithFormat:@"Yes"];
            break;
        case 1:
             eyeLevel = [NSString stringWithFormat:@"No"];
            break;
        default:
            break;
    }
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // TextView_Description.text=@"Description";
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    // [ScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height* 1.2)];
}




#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component {
    return  [dataArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (i == 0 )
    {
        typeTextfield.text = [dataArray objectAtIndex: row];
    }
    else if (i == 1 )
    {
        brandTextfield.text = [dataArray objectAtIndex: row];
    }
    else
    {
        typeOfAccountTextfield.text = [dataArray objectAtIndex:row];
    }
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[dataArray objectAtIndex: row]];
    return pickerLabel;
}



- (IBAction)TappedAction_SubmitButton:(id)sender
{
    if (typeTextfield.text.length <=0 || brandTextfield.text.length <=0 || bottlePackTextfield.text.length <=0 ||bottlePriceTextfield.text.length <= 0 || canPackTextfield.text.length <=0 || canPriceTextfield.text.length <=0 || adjcentTextfield.text.length <=0 || doorsTextfield.text.length <=0 || posTextfield.text.length <=0 || typeOfAccountTextfield.text.length <=0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please fill all the details."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    else
    {
        
            dict = [[NSMutableDictionary alloc]initWithCapacity:0];

            [dict setValue:[NSString stringWithFormat:@"%@",storeTextfield.text] forKey:@"store"];

            [dict setValue:[NSString stringWithFormat:@"%@",typeTextfield.text] forKey:@"type"];
            [dict setValue:[NSString stringWithFormat:@"%@",brandTextfield.text] forKey:@"brand"];
            [dict setValue:[NSString stringWithFormat:@"$ %@",bottlePriceTextfield.text] forKey:@"bottle"];
            [dict setValue:[NSString stringWithFormat:@"%@",bottlePackTextfield.text] forKey:@"bottlePack"];
            
            [dict setValue:[NSString stringWithFormat:@"%@",onSale] forKey:@"OnSale"];
            
            [dict setValue:[NSString stringWithFormat:@"$ %@",canPriceTextfield.text] forKey:@"CAN"];
            [dict setValue:[NSString stringWithFormat:@"%@",canPackTextfield.text] forKey:@"CanPACK"];
            
            [dict setValue:[NSString stringWithFormat:@"%@",eyeLevel] forKey:@"EyeLevel"];
            
            [dict setValue:[NSString stringWithFormat:@"%@",adjcentTextfield.text] forKey:@"Adjacent"];
            [dict setValue:[NSString stringWithFormat:@"%@",doorsTextfield.text] forKey:@"Doors"];
            [dict setValue:[NSString stringWithFormat:@"%@",posTextfield.text] forKey:@"POS"];
            [dict setValue:[NSString stringWithFormat:@"%@",typeOfAccountTextfield.text] forKey:@"TypeOfAccount"];
            
            [InfoArray addObject:dict];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!"
                                     message:@"Data Saved."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}
- (IBAction)TappedAction_ExportMail:(id)sender
{
    if (InfoArray.count <= 0)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!"
                                     message:@"No Data to export."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        comp=[[MFMailComposeViewController alloc]init];
        [comp setMailComposeDelegate:self];
        if([MFMailComposeViewController canSendMail])
        {
            [self createCSV];
            
            NSData *myData = [NSData dataWithContentsOfFile:filePath];
            if (myData)
            {
                [comp addAttachmentData:myData  mimeType:@"text/cvs" fileName:@"Records.csv"];
            }
            [comp setToRecipients:[NSArray arrayWithObjects:@"trunshrma01@gmail.com", nil]];
            [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:comp animated:YES completion:nil];
        }
    }
}

-(void)createCSV
{
    NSMutableString *csvString = [[NSMutableString alloc]initWithCapacity:0];
    [csvString appendString:@"Store Name , Type, Brand, Bottle ,Bottle Pack ,On Sale,CAN ,CAN PACK,Eye Level, Adjacent To In Cooler ?, How Many Cooler Doors ?,POS,Type Of Account,  \n\n\n\n\n\n\n\n\n\n\n\n\n"];
    
    
    for (NSDictionary *dct in InfoArray)
    {
        [csvString appendString:[NSString stringWithFormat:@"%@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@ \n",[dct valueForKey:@"store"],[dct valueForKey:@"type"],[dct valueForKey:@"brand"],[dct valueForKey:@"bottle"],[dct valueForKey:@"bottlePack"],[dct valueForKey:@"OnSale"],[dct valueForKey:@"CAN"],[dct valueForKey:@"CanPACK"],[dct valueForKey:@"EyeLevel"],[dct valueForKey:@"Adjacent"],[dct valueForKey:@"Doors"],[dct valueForKey:@"POS"],[dct valueForKey:@"TypeOfAccount"]]];
    }
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"Records.csv"];
    [csvString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}



-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if(error)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Mail will be deleted." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
        [self dismissModalViewControllerAnimated:YES];
    }
    else
    {
        [self dismissModalViewControllerAnimated:YES];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!"
                                     message:@"No Data to export."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}


- (IBAction)TappedAction_ResetButton:(id)sender
{
    typeTextfield.text = nil;
    brandTextfield.text =  nil;
    bottlePackTextfield.text = nil;
    bottlePriceTextfield.text =  nil;
    canPackTextfield.text = nil;
    canPriceTextfield.text =  nil;
    adjcentTextfield.text = nil;
    doorsTextfield.text = nil;
    posTextfield.text = nil;
    typeOfAccountTextfield.text = nil;
    storeTextfield.text = nil;

    [storeTextfield resignFirstResponder];
    [typeTextfield resignFirstResponder];
    [brandTextfield resignFirstResponder];
    [bottlePackTextfield resignFirstResponder];
    [bottlePriceTextfield resignFirstResponder];
    [canPackTextfield resignFirstResponder];
    [canPriceTextfield resignFirstResponder];
    [adjcentTextfield resignFirstResponder];
    [doorsTextfield resignFirstResponder];
    [posTextfield resignFirstResponder];
    [typeOfAccountTextfield resignFirstResponder];
}


- (IBAction)TappedAction_DoneButton:(id)sender
{
    if (i == 0 && [typeTextfield.text isEqualToString:@""])
    {
        typeTextfield.text = [dataArray objectAtIndex: 0];
    }
    if (i == 1 && [brandTextfield.text isEqualToString:@""])
    {
        brandTextfield.text = [dataArray objectAtIndex: 0];
    }
    if (i == 2 && [typeOfAccountTextfield.text isEqualToString:@""])
    {
        typeOfAccountTextfield.text = [dataArray objectAtIndex: 0];
    }
    [PickerView setHidden:YES];
}

-(void)ResignKeyboard
{
    [typeTextfield resignFirstResponder];
    [brandTextfield resignFirstResponder];
    [bottlePackTextfield resignFirstResponder];
    [bottlePriceTextfield resignFirstResponder];
    [canPackTextfield resignFirstResponder];
    [storeTextfield resignFirstResponder];
    [canPriceTextfield resignFirstResponder];
    [adjcentTextfield resignFirstResponder];
    [doorsTextfield resignFirstResponder];
    [posTextfield resignFirstResponder];
    [typeOfAccountTextfield resignFirstResponder];

}

- (IBAction)TappedAction_Brandtype:(id)sender
{
    [self ResignKeyboard];
    [PickerView setHidden:NO];

    dataArray = [NSMutableArray arrayWithObjects: @"Imports",@"Macro",@"Craft",nil];
    _datapicker.delegate = self;
    _datapicker.dataSource = self;
    i = 0;
}


- (IBAction)TappedAction_Brands:(id)sender
{
    if ([typeTextfield.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert!"
                                     message:@"Please select the import type."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        
        [PickerView setHidden:NO];
        [self ResignKeyboard];
        if ([typeTextfield.text isEqualToString:@"Imports"])
        {
            
            dataArray = [NSMutableArray arrayWithObjects: @"Corona",@"Corona Light",@"Modelo",@"Dos Equis",@"Tecate",@"Heineken",@"Heineken Light",@"Stella",@"Becks",@"Negra Modelo",@"Guiness",@"Fosters",@"Presidente",nil];
            _datapicker.delegate = self;
            _datapicker.dataSource = self;
            i = 1;
            
        }
        else if ([typeTextfield.text isEqualToString:@"Macro"])
        {
            
            dataArray = [NSMutableArray arrayWithObjects: @"Miller Lite",@"Miller High Light",@"Coors Banquet",@"Coors Light",@"Budweiser",@"Mich Ultra",@"PBR",nil];
            _datapicker.delegate = self;
            _datapicker.dataSource = self;
            i = 1;
            
        }
        else if ([typeTextfield.text isEqualToString:@"Craft"])
        {
            
            dataArray = [NSMutableArray arrayWithObjects: @"Blue Moon",@"Shock Top",@"Hoegaarden",@"Allagash White",@"UFO",@"Laguintas IPA",@"Sierra N. Pale Ale",@"Founders IPA",@"Stone IPA",@"Yuengling",@"Sam Adams",@"Rebel IPA",nil];
            _datapicker.delegate = self;
            _datapicker.dataSource = self;
            i = 1;
        }

    }
  }

- (IBAction)TappedAction_typeOfAccount:(id)sender
{
    [self ResignKeyboard];

    [PickerView setHidden:NO];
    dataArray = [NSMutableArray arrayWithObjects:@"Large Format (Grocery)",@"Package Liquor (Buy Rite)",@"C- Store (Indie/Chain)",nil];
    _datapicker.delegate = self;
    _datapicker.dataSource = self;
    i = 2;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
