//
//  ViewController.h
//  NewOne
//
//  Created by Rakesh Kumar on 14/06/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    IBOutlet UITextField *typeTextfield;
    IBOutlet UITextField *brandTextfield;
    
    IBOutlet UISegmentedControl *onSaleSegment;
    
    IBOutlet UITextField *bottlePriceTextfield;
    IBOutlet UITextField *bottlePackTextfield;
    
    IBOutlet UITextField *canPriceTextfield;
    IBOutlet UITextField *canPackTextfield;
    
    IBOutlet UISegmentedControl *eyeLevelSegment;
    
    
    IBOutlet UITextField *adjcentTextfield;
    IBOutlet UITextField *doorsTextfield;
    
    IBOutlet UITextField *posTextfield;
    IBOutlet UITextField *typeOfAccountTextfield;

    IBOutlet UIButton *exportButton;
    NSMutableArray *dataArray;
    IBOutlet UIView *PickerView;
    IBOutlet UISegmentedControl *segmentControl;
    IBOutlet UISegmentedControl *segmentControl1;
    NSString *onSale;
    
    NSString *eyeLevel;
    
    IBOutlet UITextField *storeTextfield;
}

@property (strong, nonatomic) IBOutlet UIPickerView *datapicker;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *ContentView;



@end

