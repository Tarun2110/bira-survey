//
//  AppDelegate.h
//  NewOne
//
//  Created by Rakesh Kumar on 14/06/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

